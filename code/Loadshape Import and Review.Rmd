---
title: "Load Shapes Import and Compile"
author: "Alec Stashevsky"
date: "5/21/2020"
output: html_document
---

```{r setup, warning=FALSE, results='hide',message=FALSE}
# Wrap Extra long text in PDF outpat, this line is very helpful for future Rmd setup!
knitr::opts_chunk$set(tidy.opts=list(width.cutoff=80),tidy=TRUE)
library(tidyverse)
library(lubridate)
library(data.table)
library(readxl)
library(reshape2)
library(gridExtra)
library(writexl)
library(janitor)
library(viridis)
```


# Import and clean Datasets type (5)
```{r Import datasets (5), warning=FALSE, results='hide',message=FALSE}
# Looks like we only have these three utilities now

alliant5 <- read_xlsx("//cadmusgroup.org/projects/6919-WFOE/Forecast/Demand/Copy of WPL 2019 System Load - (Alliant 5).xlsx", sheet = "System Load Shape", range = "A8:E8768") %>% setDT()

xcel5 <- read_xlsx("//cadmusgroup.org/projects/6919-WFOE/Forecast/Demand/Copy of #5  Most recent System Load shapes - (Xcel 5).xlsx", sheet = "System Load Shape Electric", range = "A9:E8769") %>% setDT()

mge5 <- read_xlsx("//cadmusgroup.org/projects/6919-WFOE/Forecast/Demand/MGE FOE Data 2020_FINAL - Cleansed - (MGE 4 & 5).xlsx", sheet = "5. Mst Rcnt Sys-Lvl Ld Shape", range = "A9:E8769") %>% setDT()

mpu5 <- read_xlsx("//cadmusgroup.org/projects/6919-WFOE/Forecast/Demand/Manitowoc WI Load Shape.xlsx", sheet = "System Load Shape", range = "A9:E8769") %>% setDT()

```



# Import and clean ATC hourly load data for years 2012-2019
```{r Import ATC Hourly Data, warning=FALSE, results='hide',message=FALSE}
# Initialize year range
years = seq(2012, 2019, 1)

# Initialize months to read in sheet names
month.num = c(1:12)

# Loop over years and months since the raw data is in a lovely format
for (i in years){
  for (j in month.num){
  assign(paste("atc", j, i, sep = "_"),
         as.data.table(na.omit(read_excel(paste0("//cadmusgroup.org/projects/6919-WFOE/Forecast/Demand/ATC Hourly Load Data/",
                                                 i,".xlsx"),
                                          sheet = month.abb[j], range = "A3:Y34"))))
  }
}

# Get list of data table names to merge
w = 1
table.names <- rep(NA, length(years)*length(month.num))
for (i in years){
  for (j in month.num){
    table.names[w] = paste("atc", j, i, sep = "_")
    w = w + 1
  }
}


# Merge on yearly data sets and perform some cleaning within the loop
## This is a very dense chunk of code 
Hour = c(0:23)
k = 0
for (i in years){
  assign(paste0("atc", i), as.data.table(melt(cbind(Hour, row_to_names(as.data.table(t(rbind(get(table.names[(1+k)]),
                                get(table.names[(2+k)]),
                                get(table.names[(3+k)]), 
                                get(table.names[(4+k)]),
                                get(table.names[(5+k)]),
                                get(table.names[(6+k)]), 
                                get(table.names[(7+k)]),
                                get(table.names[(8+k)]),
                                get(table.names[(9+k)]), 
                                get(table.names[(10+k)]),
                                get(table.names[(11+k)]),
                                get(table.names[(12+k)])
                                ))), 1)
                                ), id.vars = "Hour", variable.name = "Date", value.name = "MW")))
         
    k = k + 12     
}

# Lets add a month and a day column to help us merge later


## Test to see if we have a full 365 days in each year (and one leap year)
## Looks like we have 2017-2019 a day short and 2016 a day long, the rest are 8760 hours

unique.days.test <- data.frame(rep(NA, 8))
e = 1
for (i in years){
  unique.days.test[e] <- count(distinct(get(paste0("atc", i)), by = "Date"))
  e = e + 1
}

# Ok we so we are an extra day here or there not going to worry about that right now
##EDIT: We are missing OCTOBER 31 for 2019, 2018, 2017


# Lets make all the loadshapes the same length to merge, we will fill we NA values
atc2012.leap <- atc2012[Date != "2012-02-29"]
atc2016.leap <- atc2016[Date != "2016-02-29"]

# Convert Dates to correcty format
atc2012$Date <- ymd(atc2012$Date)
atc2013$Date <- ymd(atc2013$Date)
atc2014$Date <- ymd(atc2014$Date)
atc2015$Date <- ymd(atc2015$Date)
atc2016$Date <- ymd(atc2016$Date)
atc2017$Date <- ymd(atc2017$Date)
atc2018$Date <- ymd(atc2018$Date)
atc2019$Date <- ymd(atc2019$Date)

# Create month and Day variable to merge on 
atc2012[, `:=`(Month = month(Date), Day = day(Date))]
atc2013[, `:=`(Month = month(Date), Day = day(Date))]
atc2014[, `:=`(Month = month(Date), Day = day(Date))]
atc2015[, `:=`(Month = month(Date), Day = day(Date))]
atc2016[, `:=`(Month = month(Date), Day = day(Date))]
atc2017[, `:=`(Month = month(Date), Day = day(Date))]
atc2018[, `:=`(Month = month(Date), Day = day(Date))]
atc2019[, `:=`(Month = month(Date), Day = day(Date))]

# Ok now lets merge
atc.merge <- atc2019[atc2018, on = c("Month", "Day", "Hour")][atc2017, on = c("Month", "Day", "Hour")][atc2016, on = c("Month", "Day", "Hour")][atc2015, on = c("Month", "Day", "Hour")][atc2014, on = c("Month", "Day", "Hour")][atc2013, on = c("Month", "Day", "Hour")][atc2012, on = c("Month", "Day", "Hour")]


# clean up excesscolumns and rename add year_hour column
year_hour = c(1:8784)

atc.full <- atc.merge[, `:=`(MW_2019 = as.numeric(MW),
                             MW_2018 = as.numeric(i.MW),
                             MW_2017 = as.numeric(i.MW.1),
                             MW_2016 = as.numeric(i.MW.2),
                             MW_2015 = as.numeric(i.MW.3),
                             MW_2014 = as.numeric(i.MW.4),
                             MW_2013 = as.numeric(i.MW.5),
                             MW_2012 = as.numeric(i.MW.6))][, -c(2, 3, 6:19)][, Year_Hour := year_hour]

# looks good, but we are missing leap day for 2016 because of the merge
## lets find all NA values
na.vals <- which(is.na(atc.full))
## can impute later but I'm not going to worry about this for now
```
We are missing Ocotober 31 for 2017-2019. We are missing leap day for all years except 2012 (it is present in raw data but not merged).

##Questions for Lakin
- Alliant + MGE are labeled MW but the Xcel is labeled MWH
  - these are the same thing in this case right??
- When we merge load shapes how do we knonw if the hours are the same time i.e. what timezone are these loadshapes in?


Ok, lets plot the inidivudal loadshapes, then we will create a combination system loadshape.
```{r Individual Loadshapes}
## ALLIANT
# Plot hourly load
alliant.loadshape <- ggplot(alliant5, aes(x=`Hour (1-8760)`, y=MW)) + geom_point(alpha=0.1) + geom_smooth()+ ggtitle("Alliant Load Shape")

# Lets average the daily load to see if we can smooth this out
alliant5.avg <- unique(alliant5[, dailymaxload := max(MW), by = .(Month, Day)], by = c("Month", "Day"))

#Plot average daily load
alliant.adl <- ggplot(alliant5.avg, aes(x = as.Date(paste(alliant5.avg$Month, alliant5.avg$Day, sep = " "), format = "%m %d"), y=dailymaxload)) + geom_point(alpha=0.5) + geom_smooth() + ggtitle("Alliant Maxiumum Daily Load")

## XCEL
# Plot hourly load
xcel.loadshape <- ggplot(xcel5, aes(x=`Hour (1-8760)`, y=MWH)) + geom_point(alpha=0.1) + geom_smooth()+ ggtitle("XCEL Load Shape")

# Lets average the daily load to see if we can smooth this out
xcel5.avg <- unique(xcel5[, dailymaxload := max(MWH), by = .(Month, Day)], by = c("Month", "Day"))

#Plot average daily load
xcel.adl <- ggplot(xcel5.avg, aes(x = as.Date(paste(xcel5.avg$Month, xcel5.avg$Day, sep = " "), format = "%m %d"), y=dailymaxload)) + geom_point(alpha=0.5) + geom_smooth()+ ggtitle("Xcel Maxiumum Daily Load")

## MGE
# Plot hourly load
mge.loadshape <- ggplot(mge5, aes(x=`Hour (1-8760)`, y=MW)) + geom_point(alpha=0.1) + geom_smooth() + ggtitle("MGE Load Shape")

# Lets average the daily load to see if we can smooth this out
mge5.avg <- unique(mge5[, dailymaxload := max(MW), by = .(Month, Day)], by = c("Month", "Day"))

#Plot average daily load
mge.adl <- ggplot(mge5.avg, aes(x = as.Date(paste(mge5.avg$Month, mge5.avg$Day, sep = " "), format = "%m %d"), y=dailymaxload)) + geom_point(alpha=0.5) + geom_smooth() + ggtitle("MGE Maxiumum Daily Load")

## Adding MPU Plot - 6.29.2020
# Plot hourly load
mpu.loadshape <- ggplot(mpu5, aes(x=`Hour (1-8760)`, y=MW)) + geom_point(alpha=0.1) + geom_smooth() + ggtitle("MPU Load Shape")

# Lets average the daily load to see if we can smooth this out
mpu5.avg <- unique(mpu5[, dailymaxload := max(MW), by = .(Month, Day)], by = c("Month", "Day"))

#Plot average daily load
mpu.adl <- ggplot(mpu5.avg, aes(x = as.Date(paste(mpu5.avg$Month, mpu5.avg$Day, sep = " "), format = "%m %d"), y=dailymaxload)) + geom_point(alpha=0.5) + geom_smooth() + ggtitle("MPU Maxiumum Daily Load")


```


Now, lets combine the 8760 loadshapes *under the assumption the are the same time zone.*
```{r System Loadshape}
# First we need to merge all the data sets togeather and create a cumulative column
system <- alliant5[xcel5, on = "Hour (1-8760)"][mge5, on = "Hour (1-8760)"][mpu5, on = "Hour (1-8760)"][, c(1:6, 10, 11, 15, 16, 20, 21)][,.(`Hour (1-8760)`, Month, Day, Hour, MW.alliant = MW, dailymax.alliant = dailymaxload, MW.xcel = MWH, dailymax.xcel= i.dailymaxload,  MW.mge = i.MW, dailymax.mge = i.dailymaxload.1, MW.mpu = i.MW.1, dailymax.mpu = i.dailymaxload.2)][, `:=`(MW.system = sum(MW.alliant, MW.xcel, MW.mge, MW.mpu), dailymax.system = sum(dailymax.alliant, dailymax.xcel, dailymax.mge, dailymax.mpu)), by = `Hour (1-8760)`]

## We need to reduce the system data-set to the daily level
system.adl <- unique(system, by = c("Month", "Day"))
#Lets make a nice x axis Month -Day variable
system.adl[, `:=`(Date = as.Date(paste(system.adl$Month, system.adl$Day, 2019, sep = "-"), format = "%m-%d-%Y"))]

#ok now lets graph the full loadshape
ggplot(system, aes(x=`Hour (1-8760)`)) +
  geom_point(aes(y= MW.system), alpha=0.1) +
  geom_point(aes(y= MW.alliant), alpha=0.05, color = "red") +
  geom_point(aes(y= MW.xcel), alpha=0.05, color = "steelblue") +
  geom_point(aes(y= MW.mge), alpha=0.01, color = "magenta") +
  geom_point(aes(y= MW.mpu), alpha=0.01, color = "slateblue") +
  geom_smooth(aes(y= MW.system, color = "black")) +
  geom_smooth(aes(y= MW.alliant, color = "red")) +
  geom_smooth(aes(y= MW.xcel, color = "steelblue")) +
  geom_smooth(aes(y= MW.mge, color = "magenta")) +
  geom_smooth(aes(y= MW.mpu, color = "slateblue")) +
  scale_y_continuous(name = "MW", n.breaks = 10, limits = c(0, 4500)) +
  ggtitle("Hourly Base Loadshapes over 2019") +
  scale_color_identity(name = "Utility", 
                       breaks = c("black", "red", "steelblue", "magenta", "slateblue"), 
                       labels = c("Cummulative", "Alliant", "Xcel", "MGE", "MPU"),
                       guide = "legend") + xlab("Hour of Year (1-8760)")


# Lets plot the daily max as well
ggplot(system.adl, aes(x=Date)) +
  geom_point(aes(y= dailymax.system), alpha=0.5) +
  geom_point(aes(y= dailymax.alliant), alpha=0.5, color = "red") +
  geom_point(aes(y= dailymax.xcel), alpha=0.5, color = "steelblue") +
  geom_point(aes(y= dailymax.mge), alpha=0.5, color = "magenta") +
  geom_point(aes(y= dailymax.mpu), alpha=0.5, color = "slateblue") +
  geom_smooth(aes(y= dailymax.system, color = "black"), span = 0.2) +
  geom_smooth(aes(y= dailymax.alliant, color = "red"), span = 0.2) +
  geom_smooth(aes(y= dailymax.xcel, color = "steelblue"), span = 0.2) +
  geom_smooth(aes(y= dailymax.mge, color = "magenta"), span = 0.2)+
  geom_smooth(aes(y= dailymax.mpu, color = "slateblue"), span = 0.2)+
  scale_y_continuous(name = "MW", n.breaks = 10, limits = c(0, 4500)) +
  ggtitle("Max Daily Usage over 2019") +
  scale_color_identity(name = "Utility", 
                       breaks = c("black", "red", "steelblue", "magenta", "slateblue"), 
                       labels = c("Cummulative", "Alliant", "Xcel", "MGE", "MPU"),
                       guide = "legend")

```

Now lets, plot the ATC load shapes by year
```{r ATC Loadshapes Plot}
# Lets merge all the ATC years togeather, hopefully we can keep the trailing hours on some years

# set color scheme
colormap <- viridis(8)
#ok now lets graph the full loadshape
ggplot(atc.full, aes(x=Year_Hour)) +
  geom_point(aes(y= MW_2019), alpha=0.03, color = colormap[1]) +
  geom_point(aes(y= MW_2018), alpha=0.03, color = colormap[2]) +
  geom_point(aes(y= MW_2017), alpha=0.03, color = colormap[3]) +
  geom_point(aes(y= MW_2016), alpha=0.03, color = colormap[4]) +
  geom_point(aes(y= MW_2015), alpha=0.03, color = colormap[5]) +
  geom_point(aes(y= MW_2014), alpha=0.03, color = colormap[6]) +
  geom_point(aes(y= MW_2013), alpha=0.03, color = colormap[7]) +
  geom_point(aes(y= MW_2012), alpha=0.03, color = colormap[8]) +
  
  geom_smooth(aes(y= MW_2019, color = colormap[1])) +
  geom_smooth(aes(y= MW_2018, color = colormap[2])) +
  geom_smooth(aes(y= MW_2017, color = colormap[3])) +
  geom_smooth(aes(y= MW_2016, color = colormap[4])) +
  geom_smooth(aes(y= MW_2015, color = colormap[5])) +
  geom_smooth(aes(y= MW_2014, color = colormap[6])) +
  geom_smooth(aes(y= MW_2013, color = colormap[7])) +
  geom_smooth(aes(y= MW_2012, color = colormap[8])) +
  scale_y_continuous(name = "MW", n.breaks = 10, limits = c(5000, 10000)) +
  ggtitle("ATC Hourly Base Loadshapes over 2012-2019") +
  scale_color_identity(name = "Year", 
                       breaks = colormap,
                       labels = c("2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012"),
                       guide = "legend") + xlab("Hour of Year (1-8760)")


## Lets create and average daily max load dataset (at daily level)
atc.dml <- unique(atc.full[, `:=`(MonthDay = as.Date(paste(Month, Day, sep = "-"),
                                                 format = "%m-%d"),
                                  DML_2019 = max(MW_2019),
                                  DML_2018 = max(MW_2018),
                                  DML_2017 = max(MW_2017),
                                  DML_2016 = max(MW_2016),
                                  DML_2015 = max(MW_2015),
                                  DML_2014 = max(MW_2014),
                                  DML_2013 = max(MW_2013),
                                  DML_2012 = max(MW_2012)),
                           by = c("Month", "Day")][, -c(1, 4:12)],
                  by = c("Month", "Day"))

#Creat continuous month variable
atc.dml$MonthDay <- c(1:366)
  # as.Date(paste(atc.dml$Month, atc.dml$Day, sep= "-"), format = "%m-%d")

#Plot daily max load
ggplot(atc.dml, aes(x=MonthDay)) +
 geom_point(aes(y= DML_2019), alpha=0.5, color = colormap[1]) +
 geom_point(aes(y= DML_2018), alpha=0.5, color = colormap[2]) +
 geom_point(aes(y= DML_2017), alpha=0.5, color = colormap[3]) +
 geom_point(aes(y= DML_2016), alpha=0.5, color = colormap[4]) +
 geom_point(aes(y= DML_2015), alpha=0.5, color = colormap[5]) +
 geom_point(aes(y= DML_2014), alpha=0.5, color = colormap[6]) +
 geom_point(aes(y= DML_2013), alpha=0.4, color = colormap[7]) +
 geom_point(aes(y= DML_2012), alpha=0.5, color = colormap[8]) +
   
  geom_smooth(aes(y= DML_2019, color = colormap[1]), span = 0.2, alpha = 0.2) +
  geom_smooth(aes(y= DML_2018, color = colormap[2]), span = 0.2, alpha = 0.2) +
  geom_smooth(aes(y= DML_2017, color = colormap[3]), span = 0.2, alpha = 0.2) +
  geom_smooth(aes(y= DML_2016, color = colormap[4]), span = 0.2, alpha = 0.2) +
  geom_smooth(aes(y= DML_2015, color = colormap[5]), span = 0.2, alpha = 0.2) +
  geom_smooth(aes(y= DML_2014, color = colormap[6]), span = 0.2, alpha = 0.2) +
  geom_smooth(aes(y= DML_2013, color = colormap[7]), span = 0.3, alpha = 0.2) +
  geom_smooth(aes(y= DML_2012, color = colormap[8]), span = 0.2, alpha = 0.2) +
  scale_y_continuous(name = "MW", n.breaks = 10) +
  scale_x_continuous(name = "Day of Year", breaks = seq(1, 366, by = 30)) +
  ggtitle("ATC Max Daily Usage over 2012-2019") +
  scale_color_identity(name = "Year", 
                       breaks = colormap,
                       labels = c("2019", "2018", "2017", "2016", "2015", "2014", "2013", "2012"),
                       guide = "legend")

dev.off()
```



```{r Export}
#Lets export the cummulative load data 
write_xlsx(system, "//cadmusgroup.org/projects/6919-WFOE/Forecast/Demand/Clean Data/8760 Loadshapes (All + Cummulative).xlsx")
```
